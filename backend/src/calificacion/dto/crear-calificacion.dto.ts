import {IsNotEmpty, IsNumber} from 'class-validator'

export class CrearCalificacionDto 
{
	@IsNotEmpty()
	@IsNumber()
	puntuacion: number;
}
