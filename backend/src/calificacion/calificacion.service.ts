import { Injectable } from "@nestjs/common";
import { InjectRepository} from "@nestjs/typeorm"
import { CalificacionEntity } from "./calificacion.entity";
import { Repository, FindOneOptions } from "typeorm";

@Injectable()

export class CalificacionService
{
	constructor(
		@InjectRepository(CalificacionEntity)
		private readonly _califiacionRepository: Repository<CalificacionEntity>) {}

	async buscarPorId (id: number): Promise<CalificacionEntity> {
		return await this._califiacionRepository.findOne(id);
	}

	async buscarPorLibro (idLibro: number): Promise<CalificacionEntity[]> {
		return await this._califiacionRepository.find({
			where:{	libro: idLibro }
		});
	}

	async crear(nuevaCalificacion: CalificacionEntity): Promise<CalificacionEntity> {
		const calificacionRealizada = await this._califiacionRepository.create(nuevaCalificacion);
		return await this._califiacionRepository.save(calificacionRealizada);
	}

	async eliminar(id: number): Promise<CalificacionEntity> {
		const calificacionAEliminar = await this.buscarPorId(id);

		await this._califiacionRepository.delete(calificacionAEliminar);
		return calificacionAEliminar; 
	}
}