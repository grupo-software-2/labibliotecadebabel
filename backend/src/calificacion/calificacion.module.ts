import { Module } from "@nestjs/common";
import { CalificacionController } from "./calificacion.controller";
import { CalificacionService } from "./calificacion.service";
import { TypeOrmModule } from '@nestjs/typeorm';
import { CalificacionEntity } from "./calificacion.entity";

@Module({
	controllers: [ CalificacionController ],
	imports: [ TypeOrmModule.forFeature(
		[ CalificacionEntity ]
	)],
	providers: [ CalificacionService ]
})

export class CalificacionModule {}