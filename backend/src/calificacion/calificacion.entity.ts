import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne } from 'typeorm';
import { LibroEntity } from 'libro/libro.entity';
import { UsuarioEntity } from 'usuario/usuario.entity';

@Entity('calificacion')

export class CalificacionEntity {
	@PrimaryGeneratedColumn()
	id?: number;

	@Column({type: 'varchar', name: 'c_puntuacion'})
	puntuacion: number;

	@ManyToOne( type => LibroEntity, libro => libro.calificaciones)
	libros?: LibroEntity;

	@ManyToOne( type => UsuarioEntity, usuario => usuario.calificaciones, { eager:true })
	usuario?: UsuarioEntity;
}