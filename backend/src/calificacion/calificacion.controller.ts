import { Controller, Get, Param, Post, Body, Put, Delete, BadRequestException, Query } from "@nestjs/common";
import { CalificacionService } from "./calificacion.service";
import { CrearCalificacionDto } from "./dto/crear-calificacion.dto";
import { CalificacionEntity } from "./calificacion.entity";
import { validate } from "class-validator";

@Controller('calificacion')

export class CalificacionController
{
	constructor(
		private readonly _calficacionService: CalificacionService) {}

	@Get(':id')
	async obtenerUno(
		@Param('id') id): Promise<CalificacionEntity> {
		return this._calficacionService.buscarPorId(id);
	}

	@Get('obtenerOA/:idLibro')
	async obtenerOA(
		@Param('idLibro') idLibro):Promise<CalificacionEntity[]>{
		return this._calficacionService.buscarPorLibro(idLibro);
	}

	@Post()
	async crear(
		@Body() nuevaCalificacion: CrearCalificacionDto ): Promise<CalificacionEntity> {
		return this._calficacionService.crear(nuevaCalificacion);
	}

	@Delete(':id')
	async borrar(
		@Param('id') id: number): Promise<CalificacionEntity> {
		return this._calficacionService.eliminar(id);
	}
}