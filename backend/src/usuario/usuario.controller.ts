import { Controller, Get, Param, Post, Body, Put, Query } from "@nestjs/common";
import { UsuarioService } from "./usuario.service";
import { CrearUsuarioDto } from "./dto/crear-usuario.dto";
import { validate } from "class-validator";

@Controller('usuario')

export class UsuarioController 
{
	constructor(private readonly _usuarioService: UsuarioService) { }

    @Get()
    async obtenerTodos(): Promise<CrearUsuarioDto[]> {
        return await this._usuarioService.buscarTodos();
    }

    @Get('obtenerPorId/:id')
    async obtenerPorId(
        @Param() id): Promise<CrearUsuarioDto> {
        return await this._usuarioService.buscarPorId(id);
    }

    @Post('nuevo')
    async crear(
        @Body() usuarioACrear): Promise<CrearUsuarioDto> {
        return await this._usuarioService.crear(usuarioACrear);
    }

    @Put(':id')
    async actualizar(
        @Param('id') id,
        @Body() usuarioAActualizar): Promise<CrearUsuarioDto> {
        return this._usuarioService.actualizar(id, usuarioAActualizar);
    }

    @Get()
    async obtenerPorQuery(
        @Query() consulta: object): Promise<CrearUsuarioDto[]> {
        return await this._usuarioService.query(consulta);
    }
}