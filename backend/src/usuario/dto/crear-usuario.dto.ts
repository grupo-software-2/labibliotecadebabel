import { IsNotEmpty, IsAlphanumeric, IsInt } from 'class-validator';

export class CrearUsuarioDto 
{
	@IsNotEmpty()
	@IsAlphanumeric()
	user:string;

	@IsNotEmpty()
	@IsAlphanumeric()
    password:string;

    @IsNotEmpty()
    @IsInt()
    tipoUsuario: number;
}