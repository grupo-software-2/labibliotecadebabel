import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { LibroEntity } from 'libro/libro.entity';
import { CalificacionEntity } from 'calificacion/calificacion.entity';

enum roles {
    administrador = "administrador",
    lector = "lector"
}

@Entity('usuario')

export class UsuarioEntity 
{
	@PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', name: 'usuario_user', unique: true})
    user: string;

    @Column({ type: 'varchar', name: 'usuario_password' })
    password: string;

    @Column({ type: 'enum', name: 'usuario_tipo_usuario', enum: roles })
    tipoUsuario: number;

    @OneToMany(type => LibroEntity, libro => libro.usuario)
    libros?: LibroEntity[];

    @OneToMany(type => CalificacionEntity, calificacion => calificacion.usuario)
    calificaciones?: CalificacionEntity[];
}