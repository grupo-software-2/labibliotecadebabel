import { Injectable } from "@nestjs/common";
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioEntity } from "./usuario.entity";
import { Repository } from "typeorm";

@Injectable()

export class UsuarioService 
{
	constructor(
        @InjectRepository(UsuarioEntity)
        private readonly _usuarioRepository: Repository<UsuarioEntity>,
    ) {}

    async actualizar(id: number, usuarioAEditar: UsuarioEntity):Promise<UsuarioEntity>{     
        const usuarioEditado = await this._usuarioRepository.update(id, usuarioAEditar);

        return this.buscarPorId(id);
    }

    async buscarPorId(id: number): Promise<UsuarioEntity> {
        return await this._usuarioRepository.findOne(id);
    }

    async buscarTodos(): Promise<UsuarioEntity[]> {
        const obtenerTodosConPromesa = this._usuarioRepository.find({
            order: {
                id: 'DESC'
            }
        })

        return obtenerTodosConPromesa;
    }

    async crear(nuevoUsuario: UsuarioEntity): Promise<UsuarioEntity> {
    	const usuarioCreado = await this._usuarioRepository.create(nuevoUsuario);
        return this._usuarioRepository.save(usuarioCreado)
    }

    async query(consulta: object): Promise<UsuarioEntity[]> {

        return await this._usuarioRepository.find({
            order: {
                id: 'DESC'
            },

            where: consulta
        })
    }
}