import {IsNotEmpty, IsString, IsNumberString} from 'class-validator';

export class CrearLibroDto 
{
	@IsNotEmpty()
	@IsString()
	titulo: string;

	@IsNotEmpty()
	@IsString()
	autor: string;

	@IsNotEmpty()
	@IsString()
	sinopsis: string;
}