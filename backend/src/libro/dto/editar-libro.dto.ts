import {IsNotEmpty, IsOptional, IsString, IsNumberString} from 'class-validator';

export class EditarLibroDto 
{
	@IsNotEmpty()
	@IsString()
	titulo: string;

	@IsNotEmpty()
	@IsString()
	autor: string;

	@IsNotEmpty()
	@IsString()
	sinopsis: string;
}