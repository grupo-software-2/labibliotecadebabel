import { Injectable } from "@nestjs/common";
import { InjectRepository} from "@nestjs/typeorm";
import { LibroEntity } from "./libro.entity";
import { Repository } from "typeorm";

@Injectable()

export class LibroService 
{
	constructor(
		@InjectRepository(LibroEntity)
		private readonly _libroRepository: Repository<LibroEntity>
	) {}

	async buscarPorId(id: number): Promise<LibroEntity> {
		return await this._libroRepository.findOne(id);
	}

	async buscarTodos(): Promise<LibroEntity[]> {
		const obtenerTodosConPromesa = await this._libroRepository.find({
			order: {
				id: 'DESC'
			}
		})

		return obtenerTodosConPromesa;
	}

	async crear(nuevoLibro: LibroEntity): Promise<LibroEntity> {
		const libroCreado = await this._libroRepository.create(nuevoLibro);

		return this._libroRepository.save(libroCreado);
	}

	async editar(id: number, libroAEditar: LibroEntity): Promise<LibroEntity> {
		const libroEditado = await this._libroRepository.update(id, libroAEditar);

		return this.buscarPorId(id);
	}

	async query(consulta: object): Promise<LibroEntity[]> {
		return await (this._libroRepository.find({
			order: {
				id: 'DESC'
			},

			where: consulta
		}))
	}
}