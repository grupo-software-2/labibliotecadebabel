import { Controller, Get, Param, Post, Body, Put, Query,  UseInterceptors, UploadedFile, 
	FileInterceptor, Res, UploadedFiles, FilesInterceptor } from "@nestjs/common";
import { LibroService } from "./libro.service";
import { CrearLibroDto } from "./dto/crear-libro.dto";
import { EditarLibroDto } from "./dto/editar-libro.dto";
import { editarLibro } from "./funciones/editar-libro";
import { validate } from "class-validator";

@Controller('libro')

export class LibroController 
{
	constructor(private readonly _libroService: LibroService) {}

	@Get()
	async obtenerTodos() {
		return await this._libroService.buscarTodos();
	}

	@Get(':id')
	async obtenerUno(
		@Param('id') id): Promise<CrearLibroDto> {
		return await this._libroService.buscarPorId(id);
	}

	@Post('nuevo')
	async crear(
		@Body() nuevoLibro): Promise<CrearLibroDto> {
		return await this._libroService.crear(nuevoLibro);
	}

	@Post('cargar-libro')
	@UseInterceptors(FilesInterceptor('libro'))
	cargarLibro(@UploadedFile() libroACargar) {
		return libroACargar;
	}

	@Put(':id')
	async editar(
		@Param() id: number,
		@Body() libroAEditar): Promise<EditarLibroDto> {
		const editarUnLibro =  editarLibro(libroAEditar);
		const arregloDeErrores = await validate(editarUnLibro);
		const existenErrores = arregloDeErrores.length > 0;

		if(existenErrores)
		{
			console.log('Errores al editar el libro');
		}

		else 
		{
			return await this._libroService.editar(id, libroAEditar);
		}
	}

	@Get()
	async obtenerPorQuery(
		@Query() consulta: object): Promise<CrearLibroDto[]> {
		return await this._libroService.query(consulta);
	}

}