import { EditarLibroDto } from "../dto/editar-libro.dto";

export function editarLibro(libro: EditarLibroDto) :EditarLibroDto 
{
    const libroAEditar = new EditarLibroDto()

    Object.keys(libro).map(atributo => {
        libroAEditar[atributo]=libro[atributo]
    })
    
    return libroAEditar
}