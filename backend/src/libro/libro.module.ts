import { Module, MulterModule } from "@nestjs/common";
import { LibroController } from "./libro.controller";
import { TypeOrmModule } from '@nestjs/typeorm';
import { LibroEntity } from "./libro.entity";
import { LibroService } from "./libro.service";

@Module({
	controllers: [ LibroController ],
	imports: [
		TypeOrmModule.forFeature(
			[ LibroEntity ]
		),

		MulterModule.register({
			dest: './libros'
		})
	],
	providers: [ LibroService ]
})

export class LibroModule {}