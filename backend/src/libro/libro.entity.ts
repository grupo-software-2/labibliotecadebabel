import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { CalificacionEntity } from 'calificacion/calificacion.entity';
import { UsuarioEntity } from 'usuario/usuario.entity';

@Entity('libro')

export class LibroEntity 
{
	@PrimaryGeneratedColumn()
	id?: number;

	@Column({ type: 'varchar', name: 'libro_titulo'})
	titulo: string;

	@Column({ type: 'varchar', name: 'libro_autor'})
	autor: string;

	@Column({ type: 'varchar', name: 'libro_sinopsis'})
	sinopsis: string;

	@OneToMany( type => LibroEntity, libro => libro.calificaciones)
	calificaciones?: LibroEntity[];

	@ManyToOne( type => UsuarioEntity, usuario => usuario.calificaciones, { eager:true })
	usuario?: UsuarioEntity;
}