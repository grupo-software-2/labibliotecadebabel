import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CalificacionModule } from 'calificacion/calificacion.module';
import { LibroModule } from 'libro/libro.module';
import { UsuarioModule } from 'usuario/usuario.module';
import { CalificacionEntity } from 'calificacion/calificacion.entity';
import { LibroEntity } from 'libro/libro.entity';
import { UsuarioEntity } from 'usuario/usuario.entity';

@Module({
  imports: [
    CalificacionModule,
  	LibroModule,
  	UsuarioModule,
  	TypeOrmModule.forRoot({
  	  type: 'mysql',
      host: '192.168.99.100',
      port: 32768,
   	  username: 'admin',
      password: '12345678',
      database: 'labibliotecadebabel',
      entities:[
          CalificacionEntity,
      	  LibroEntity,
      	  UsuarioEntity
      ],
      synchronize: true
  	})
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
